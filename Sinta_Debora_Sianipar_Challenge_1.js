// soal no 1
function changeWord (selectedText, changeText, text){
  if(selectedText[0] === selectedText[0].toUpperCase()){ // cek apakah huruf pertama dari kata yang mau diganti itu huruf besar
    changeText = changeText[0].toUpperCase() + changeText.slice(1); // ganti huruf pertama dari kata penggantinya huruf besar
  }else {
    changeText = changeText[0].toLowerCase() + changeText.slice(1); // jika tidak maka huruf pertama dari kata penggantinya  ganti dengan huruf kecil
  }
  return text.replaceAll(selectedText,changeText); // replaceAll() adalah modul bawaan js untuk mereplace semua yang sama dengan parameter yang diberikan
}

const kalimat1 = 'Andini sangat mencintai kamu selamanya'
const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu'

console.log(changeWord('mencintai','membenci',kalimat1))

console.log(changeWord('bromo','semeru',kalimat2))

// soal no 2
function checkTypeNumber(givenNumber) {
console.log(typeof givenNumber)
if (typeof givenNumber == "string" || typeof givenNumber == "undefined"
|| typeof givenNumber == null || typeof givenNumber == "object") {
    return "Tipe Data Invalid"
 } else if (givenNumber % 2 == 0) {
   return "Genap"
} else {
  return "Ganjil"
}
}

console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber())

// soal no 3
function checkEmail(email=-1) {
  let at =/[@]/g
  let domain = /.com|.co.id/g
  if(email==-1)
    return "ERROR"
  if(email.search(at)!=-1){
  if(email.search(domain)!=-1){
    return "VALID"
  }else{
    return "INVALID"
  }
}
return "ERROR"
}

console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar')) 
console.log(checkEmail('apranata')) // string bernilai eror karena pada syntax kode tidak ada @.com atau @.co.id
console.log(checkEmail('3322'))// string bernilai eror karena pada syntax kode tidak ada @.com atau @.co.id dan juga angka tidak termasuk pola pada syntax ini
console.log(checkEmail()) // string bernilai eror karena nilai pada parameter kosong


// soal no 4
function isValidPassword (givenPassword) {
  const regex = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])/;
  if (typeof givenPassword === 'string'){
    if (regex.test(givenPassword) && givenPassword.length >= 8) {
      return true;
    }else{
      return false;
    }
  }else if (typeof givenPassword === 'undefined'){
    return 'ERROR : Tolong Masukkan Password' 
  }else {
    return 'ERROR : Invalid Data Type'
  }
}

console.log(isValidPassword('Meong2021')) // karena memenuhi kriteria, yakni terdiri dari 8 huruf, ada huruf besar, ada huruf kecil, dan ada angka.
console.log(isValidPassword('meong2021')) // false karena tidak ada huruf besar
console.log(isValidPassword('@eong')) // false karena hanya terdiri dari 5 huruf
console.log(isValidPassword('Meong2')) // false karena hanya terdiri dari 6 huruf
console.log(isValidPassword('0')) // false karena hanya terdiri dari 1 angka saja, tidak memiliki huruf besar maupun huruf kecil
console.log(isValidPassword('')) // false karena nilai parameter kosong.

// soal no 5
const getSplitName = (personName) => {
    if(typeof personName === "string"){
        personName = personName.split(" ")
        if(personName.length == 3){
            return ("firstName: " + personName[0] + " , " + "middleName: " + personName[1] + " , " + "lastName: " + personName[2])
        } else if (personName.length == 2){
            return ("firstName: " + personName[0] + " , " + "middleName: " + personName[1] + " , " + "lastName: " + null)
        } else if (personName.length == 1){
            return ("firstName: " + personName[0] + " , " + "middleName: " + null + " , " + "lastName: " + null)
        } else {
           return Error("This Function Just Only for 3 character") 
        }
    } else {
        return Error("Invalid Data Type")
    }
}

console.log(getSplitName("Aldi Daniela Pranata"))
console.log(getSplitName("Aldi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))


// soal no 6
function getAngkaTerbesarKedua(givenNumber){
    if(typeof givenNumber == "object"){
        givenNumber.sort(function(a, b){return b-a})
        return givenNumber[1]
    } else {
        return Error("Invalid data type")
    }
}
const dataAngka = [9,4,7,7,4,3,2,2,8]
console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())

// soal no 7
const dataPenjualanPakAldi = [
{
    namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
    hargaSatuan : 760000,
    kategori : "Sepatu Sport",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Tristan Black Brown High',
    hargaSatuan : 960000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 37,
},
{
    namaProduct : 'Sepatu Warrior Tristan Maroon High',
    hargaSatuan : 360000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
    hargaSatuan : 120000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
}
]
const getTotalPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
        return string = 'Invalid type data'
    }
    else {
        let totalPrice = dataPenjualan.reduce(function (accumulator, item) {
            return accumulator + item.totalTerjual;
          }, 0);
    
        return totalPrice
    }
}

console.log(getTotalPenjualan(dataPenjualanPakAldi))
console.log(getTotalPenjualan())

// soal no 8
const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];

const currencyFormatting = formatted => {
    return Intl.NumberFormat("id-ID", {
        style: "currency", currency: "IDR" 
    }).format(formatted)
}

const findMax = arr  =>  {
    let max = Math.max.apply(Math, arr.map(function(o) { return o.totalTerjual; }))
    objIndex = arr.findIndex((obj => obj.totalTerjual == max));
    return objIndex
}

const getInfoPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
        return string = "Invalid data type"
    }
    else {
        let totalKeuntungan = dataPenjualan.reduce(function (untungTemp, item) {
            return untungTemp + ((item.hargaJual * item.totalTerjual - item.hargaBeli * (item.totalTerjual + item.sisaStok)));
          }, 0);
        let totalModal = dataPenjualan.reduce(function (modalTemp, item) {
            return modalTemp + (item.hargaBeli * (item.totalTerjual + item.sisaStok));
          }, 0);
        
          
        let persentaseKeuntungan = ((totalKeuntungan / totalModal) * 100).toFixed(0) + "%"
        findMax(dataPenjualan)
          
        return {
            totalKeuntungan: currencyFormatting(totalKeuntungan),
            totalModal: currencyFormatting(totalModal),
            persentaseKeuntungan: persentaseKeuntungan,
            produkBukuTerlaris: dataPenjualan[objIndex].namaProduk,
            penulisTerlaris: dataPenjualan[objIndex].penulis
        }
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel))
console.log(getInfoPenjualan())
